import React from 'react';
import Products from '../../components/products/products';
import Header from '../../components/header/header';
import './style.css';

const grid = () => {
    return (
        <div className="grid">
            <Header title="Products" useIcon={true} />
            <Products />
        </div>
    )
}

export default grid;