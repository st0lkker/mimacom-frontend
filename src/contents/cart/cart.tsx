import React, { useContext, useEffect, useReducer, useState } from 'react';
import axios from 'axios';
import Header from '../../components/header/header';
import Items from './items/items';
import { StoreContext } from '../../contents/store';
import './style.css';

export const ACTIONS = {
    INCREMENT: "increment",
    DECREMENT: "decrement",
    PUSH: "push",
    REMOVE: "remove",
    DEFAULT: "",
}

let isIncrementAmount = true;

// Cart interface
interface ICart { id?: string, name?: string, price: number, image?: string, stock: number, activeStock: number }

// Get the cart from the session storage if possible, then parse it to an array of objects if possible
const sessionCart: string = sessionStorage.getItem("SESSION_CART") || '';
const defaultCart: ICart[] = sessionCart && sessionCart.length ? JSON.parse(sessionCart) : [];

// Reducer function for the useReducer hook
function reducer(cartItems: ICart[], action: any) {
    switch (action.type) {
        case ACTIONS.INCREMENT:
            isIncrementAmount = true;
            const incrProduct = cartItems.find(item => item.id === action.payload.id);

            if (incrProduct && incrProduct.stock > 0) {
                const incrStock = incrProduct.stock - 1;

                axios.patch('http://localhost:3000/grocery/' + incrProduct.id, { stock: incrStock })
                    .then(response => {
                        action.payload.callback({ ...response.data, increment: true })
                    })
            }
            break;
        case ACTIONS.DECREMENT:
            isIncrementAmount = false;
            const decrProduct = cartItems.find(item => item.id === action.payload.id);

            if (decrProduct) {
                const decrStock = decrProduct.stock + 1;
                axios.patch('http://localhost:3000/grocery/' + decrProduct.id, { stock: decrStock })
                    .then(response => {
                        action.payload.callback({ ...response.data, increment: false })
                    })
            }
            break;
        case ACTIONS.PUSH:
            return [...cartItems, { ...action.payload.cartItem, activeStock: 1 }]
        case ACTIONS.REMOVE:
            return [...cartItems.filter(item => item !== action.payload.currentProductRemove)]
        default:
            return [...cartItems];
    }

    return cartItems;
}

const Cart = () => {
    const [cartItems, dispatch] = useReducer(reducer, defaultCart);
    const [activeProduct, setActiveProduct] = useContext(StoreContext);

    useEffect(() => {
        if (activeProduct.id === undefined) {
            return;
        }

        const currentProduct = cartItems.find((item: ICart) => item.id === activeProduct.id)
        if (currentProduct && currentProduct.activeStock === 0) {
            dispatch({ type: ACTIONS.REMOVE, payload: { currentProductRemove: currentProduct } });
            return;
        }
        if (currentProduct && isIncrementAmount) {
            currentProduct.activeStock += 1;
            dispatch({ type: ACTIONS.DEFAULT });
            return;
        }
        if (currentProduct && !isIncrementAmount) {
            currentProduct.activeStock -= 1;
            dispatch({ type: ACTIONS.DEFAULT });
            return;
        }

        dispatch({ type: ACTIONS.PUSH, payload: { cartItem: activeProduct } });
    }, [activeProduct])

    useEffect(() => {
        sessionStorage.setItem("SESSION_CART", JSON.stringify(cartItems));
    }, [cartItems])

    const totalAmount = cartItems.reduce((acc: number, item: ICart) => {
        acc += item.price * item.activeStock;
        return acc;
    }, 0)

    return (
        <div className="cart">
            <Header title="Cart" useIcon={false} />
            {cartItems.length !== 0 ?
                <div>
                    {cartItems.map(item => (<Items
                        key={item.id}
                        id={item.id}
                        image_url={item.image_url}
                        stock={item.activeStock}
                        productName={item.productName}
                        price={item.price}
                        dispatch={dispatch}
                        callback={setActiveProduct}
                    />))}
                    <div className="cart-total-amount">
                        <p>Total Amount:</p><span>$ {totalAmount}</span>
                    </div>
                    <div className="cart-payment">
                        <button>Make a payment</button>
                    </div>
                </div> :
                <div className="cart-empty">
                    <i className="fas fa-shopping-cart"></i>
                    <p>Cart is empty...</p>
                </div>}


        </div>
    )
}

export default Cart;