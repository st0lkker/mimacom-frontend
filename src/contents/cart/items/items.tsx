import React, { FunctionComponent } from 'react';
import { ACTIONS } from '../cart';
import './style.css';

type Props = {
    id: string,
    image_url: string,
    stock: Number,
    productName: string,
    price: Number,
    dispatch: any,
    callback: any
}

const items: FunctionComponent<Props> = ({ id, image_url, stock, productName, price, dispatch, callback }) => {
    return (
        <div className="cart-items">
            <div className="item-image">
                <img src={image_url} alt={productName} />
            </div>
            <div className="item-info-wrapper">
                <div className="item-info">
                    <p>{productName}</p>
                    <p className="item-price">$ {price}</p>
                </div>
                <div className="item-quantity">
                    <span onClick={() => dispatch({ type: ACTIONS.DECREMENT, payload: { id, callback } })}>-</span>
                    <span>{stock}</span>
                    <span onClick={() => dispatch({ type: ACTIONS.INCREMENT, payload: { id, callback } })}>+</span>
                </div>
            </div>
        </div>
    )
}

export default items;