import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Header from '../../components/header/header';
import Product from '../../components/products/product/product';

const Wishlist = () => {
    const [products, setProducts] = useState<any[]>([]);
    useEffect(() => {
        axios.get('http://localhost:3000/grocery?favorite=1')
            .then(response => {
                setProducts([...response.data])
            })
    }, [])

    return (
        <div className="grid">
            <Header title="Wishlist" useIcon={false} />
            <div className="products">
                {products.map(product => (<Product
                    key={product.id}
                    id={product.id}
                    image_url={product.image_url}
                    stock={product.stock}
                    productName={product.productName}
                    price={product.price}
                    productDescription={product.productDescription}
                    favorite={product.favorite} />))}
            </div>
        </div>
    )
}

export default Wishlist;