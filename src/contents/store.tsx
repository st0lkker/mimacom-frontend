import React, { createContext, useState } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Grid from './grid/grid';
import Cart from './cart/cart';
import Wishlist from './wishlist/wishlist';
import './style.css';

type IActiveProduct = { id?: string, name?: string, price?: number, image?: string, stock?: number }
type IStoreContext = [IActiveProduct, React.Dispatch<React.SetStateAction<IActiveProduct>>]

export const StoreContext = createContext<IStoreContext>([{}, () => ({})])

const Store = () => {
    const [activeProduct, setActiveProduct] = useState<IActiveProduct>({});
    return (
        <BrowserRouter>
            <StoreContext.Provider value={[activeProduct, setActiveProduct]}>
                <div className="store-wrapper">
                    <div className="grid-wrapper">
                        <Switch>
                            <Route path="/" exact component={Grid} />
                            <Route path="/wisthlist" exact component={Wishlist} />
                            <Route path="/cart" exact component={Cart} />
                        </Switch>
                    </div>
                    <div className="cart-wrapper">
                        <Route path="/" component={Cart} />
                    </div>
                </div>
            </StoreContext.Provider>
        </BrowserRouter>
    )
}

export default Store;