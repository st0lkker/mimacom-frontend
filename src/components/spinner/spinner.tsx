import React from 'react';
import './style.css';

const spinner = () => {
    return (
        <div className="wrapper-spinner">
            <div className="spinner">
                <div className="lds-roller">
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                </div>
            </div>
        </div>
    )
}

export default spinner;