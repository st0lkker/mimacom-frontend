import React, { FunctionComponent } from 'react';
import { NavLink } from 'react-router-dom';
import './style.css';

type Props = {
    title: string
    useIcon: Boolean
}

const header: FunctionComponent<Props> = ({ title, useIcon }) => {
    return (
        <header>
            {useIcon ? <NavLink to={"/wisthlist"} className="wisthlist-link">
                <i className="far fa-heart"></i>
            </NavLink> : <NavLink to={"/"} >
                <i className="fas fa-arrow-left"></i>
            </NavLink>}
            <h1>{title}</h1>
            <NavLink to={"/cart"} className="cart-link">
                <i className="fas fa-shopping-cart"></i>
            </NavLink>
        </header>
    )
}

export default header;