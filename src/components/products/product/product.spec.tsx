import { render } from '@testing-library/react';

import Product from './product';

test('try to render component', () => {
    render(<Product id="id" image_url="enlace" stock={20} productName="Nombre" price={50} productDescription="Descripcion" favorite={true} />);
});
