import React, { FunctionComponent } from 'react';
import AddToCart from '../../addToCart/addToCart';
import './style.css';

type Props = {
    id: string,
    image_url: string,
    stock: Number,
    productName: string,
    price: Number,
    productDescription: string,
    favorite: Boolean
}

const products: FunctionComponent<Props> = ({ id, image_url, stock, productName, price, productDescription, favorite }) => {

    return (
        <div className="product-wrapper">
            <div>
                <img alt={productName} src={image_url} />
            </div>

            <div className="product-info">
                <div className="product-section1">
                    <div className="section-1">
                        <p className="name">{productName}</p>
                        <p className="description">{productDescription}</p>
                    </div>
                    <div className="section-2">
                        <span className="price"><span>$ </span>{price}</span>
                    </div>
                </div>
                <div className="product-section2">
                    <div className="stock">{stock} Left</div>
                    <div>
                        <AddToCart id={id} stock={stock} />
                    </div>
                </div>
            </div>

            <div className="product-favorite">
                {favorite ? <i className="fas fa-heart"></i> : <i className="far fa-heart"></i>}
            </div>
        </div>
    )
}

export default products;