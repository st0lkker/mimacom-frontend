import axios from 'axios';
import React, { useEffect, useState, useContext } from 'react';
import { StoreContext } from '../../contents/store';
import Product from './product/product';
import Spinner from './../spinner/spinner';

const Products = () => {
    const [products, setProducts] = useState<any[]>([]);
    const [activeProduct] = useContext(StoreContext);
    const [loadingProducts, setLoadingProducts] = useState<Boolean>(false);

    useEffect(() => {
        setLoadingProducts(true);
        axios.get('http://localhost:3000/grocery')
            .then(response => {
                setProducts([...response.data])
                setLoadingProducts(false);
            })
    }, []);

    useEffect(() => {
        if (activeProduct.id !== undefined) {
            const currentProduct = products.find(product => product.id === activeProduct.id)
            currentProduct.stock = activeProduct.stock;
            setProducts([...products]);
        }
    }, [activeProduct])

    return (
        <div className="products">
            { loadingProducts ? <Spinner /> :
                products.map(product => (<Product
                    key={product.id}
                    id={product.id}
                    image_url={product.image_url}
                    stock={product.stock}
                    productName={product.productName}
                    price={product.price}
                    productDescription={product.productDescription}
                    favorite={product.favorite} />))
            }

        </div>
    )
}

export default Products;