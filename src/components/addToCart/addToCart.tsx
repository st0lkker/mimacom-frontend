import React, { FunctionComponent, useContext } from 'react';
import axios from 'axios';
import { StoreContext } from '../../contents/store';
import './style.css';

type Props = {
    id: string,
    stock: Number
}
const AddToCart: FunctionComponent<Props> = ({ id, stock }) => {
    const [, setActiveProduct] = useContext(StoreContext);
    const addToCartHandler = () => {
        if (stock > 0) {
            axios.patch('http://localhost:3000/grocery/' + id, { stock: +stock - 1 })
                .then(response => {
                    setActiveProduct({ ...response.data })
                })
        }
    }

    return (
        <div className="wrapper-cart-button">
            <button onClick={() => addToCartHandler()}>
                <i className="fas fa-cart-plus"></i>
                <span className="add-cart">add</span>
            </button>
        </div>

    )
}

export default AddToCart;